import os
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('op_map')


def test_containers(host):
    with host.sudo():
        app = host.docker("op-map")
        database = host.docker("op-map-database")
        assert app.is_running
        assert database.is_running


def test_application_ui(host):
    cmd = host.run("curl --insecure https://op-map-default/")
    assert "<title>ESS OP-Map</title>" in cmd.stdout
